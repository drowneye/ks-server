const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const session = require('express-session');
const passport = require('passport');
const connection = require('./config/database');

// Main Routes 
const mainRoute = require('./routes/mainRoute');
const newCtgRoute = require('./routes/newCatalogRoute');
const usersRoute = require('./routes/usersRoute');
const orderRoute = require('./routes/orderRoute');
const profileRoute = require('./routes/profileRoute');

// Admin Routes
const admCategory = require('./routes/adminCategoriesRoute'); 
const admProduct = require('./routes/admProduct');
const admUsers = require('./routes/admUsers');
const admOrder = require('./routes/adminOrdersRoute');
const admProperty = require('./routes/admProperty');
const admDelivery = require('./routes/admDelivery');

// Other modules
const errPage = require('./routes/errorPagesRoute');
const auth = require('./routes/authMw');

const MongoStore = require('connect-mongo')(session);

//Доступ к переменным в .env файле (procces.env.VAR_NAME)
require('dotenv').config();

// Создание Express приложения
const app = express();

app.use(express.urlencoded({extended: true}));
app.use(express.json());

// Загрузка View Engine
app.set('views', path.join(__dirname, 'views') );
app.set('view engine', 'pug');

// Подключение сессий
const sessionStore = new MongoStore({
    mongooseConnection: connection,
    collection: 'sessions'
});
app.use(session({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: true,
    store: sessionStore,
    cookie: {
        maxAge: 1000 * 60 * 60 * 24
    }
}));


// -------PASSPORT AUTH---------
require('./config/passport');

app.use(passport.initialize());
app.use(passport.session());
app.use((req, res, next) => {
    //console.log(req.session);
    //console.log(req.user);
    global.user = req.user;
    next();
});

app.locals.basedir = path.join(__dirname, 'views');

// ------------ВЫЗОВ МОДЕЛЕЙ СХЕМ------------
const Category = require('./models/category');


// ------------РОУТЫ ПРИЛОЖЕНИЯ------------
//app.use((req,res,next)=>{ console.log('string for debug');  next();})

app.use(async (req, res, next) => {
    let categoryList = await Category.find({}, (err) => {
        if (err) throw err;
    }); 
    global.categories = categoryList;
    next()
}) 

app.use(mainRoute);
app.use(newCtgRoute);

app.use('/users', usersRoute);
app.use('/users', orderRoute);
app.use('/users', auth.isAuth, profileRoute);

app.get('/admin', auth.isManager, (req, res) => {
  res.render('ap-navigation', {
    title: 'Администрирование'
  })
});

app.use('/admin', auth.isManager, admCategory);
app.use('/admin', auth.isManager, admProduct);
app.use('/admin', auth.isManager, admUsers);
app.use('/admin', auth.isManager, admOrder);

app.use('/admin', auth.isAdmin, admProperty);
app.use('/admin', auth.isAdmin, admDelivery);
app.use(errPage);

// ----------SERVER-------------
async function startServer(){
    try {
        
        //подключение к БД маркет 
        await mongoose.connect(process.env.DB_STRING, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true,   
        }); 
        // Запуск сервера 
        app.listen(process.env.PORT || 3000, () => {
            console.log(`Server: Online;  Port: ${process.env.PORT || 3000} `);
        });
    } catch (err){
        console.log('ОШИБКА: отсутствует подключение к базе данных. Запуск Сервера невозможен.')
        console.error(err);
    }
};

startServer();

app.use(express.static('public'));
