const sh = () => {
  const mrcSHTitles = Array.from(document.querySelectorAll('[data-mrc-sh-title]'));
  const mrcSHBodies = Array.from(document.querySelectorAll('[data-mrc-sh-body]'));
  mrcSHTitles.forEach((title) => {
    const itemObj = { name: title, bodies: [] };
    const itemName = title.dataset.mrcShTitle;
    const bodiesList = [];
    mrcSHBodies.forEach((body) => {
      const itemBody = body;
      const bodyName = itemBody.dataset.mrcShBody;
      if (itemName === bodyName) {
        bodiesList.push(itemBody);
        itemBody.style.display = 'none';
      }
    });
    if (bodiesList) {
      itemObj.bodies = bodiesList;
      itemObj.name.addEventListener('click', (e) => {
        e.preventDefault();
        bodiesList.forEach((item) => {
          const currentItem = item;
          currentItem.style.display = currentItem.style.display === 'none' ? '' : 'none';
        });
      });
    }
  });
};

const productBuy = () => {
  const buyBtn = Array.from(document.querySelectorAll('.product-price__btn a'));
  const cart = document.querySelector('.header-cart__count');
  if (!localStorage.basket) localStorage.setItem('basket', JSON.stringify({}));
  buyBtn.forEach((btn) => {
    btn.addEventListener('click', (e) => {
      e.preventDefault();
      const basket = localStorage.basket ? JSON.parse(localStorage.basket) : {};
      const pId = e.target.dataset.id;
      basket[pId] = basket[pId] ? basket[pId] : {
        img: e.target.dataset.img,
        name: e.target.dataset.name,
        cost: e.target.dataset.cost,
      };
      basket[pId].count = basket[pId].count ? basket[pId].count += 1 : 1;
      const bArr = Object.values(basket);
      let cartCounter = 0;
      bArr.forEach((item) => {
        cartCounter += item.count;
      });
      cart.innerText = cartCounter;
      localStorage.setItem('basket', JSON.stringify(basket));
    });
  });
};

const headerCart = () => {
  const cart = document.querySelector('.header-cart__count');
  document.addEventListener('DOMContentLoaded', () => {
    if (localStorage.basket) {
      const basket = JSON.parse(localStorage.basket);
      const bArr = Object.values(basket);
      let cartCounter = 0;
      bArr.forEach((item) => {
        cartCounter += item.count;
      });
      if (cartCounter > 0) cart.innerText = cartCounter;
    }
  });
};

const formUser = () => {
  const form = document.forms['form-user'];
  if (form) {
    const { edit } = form;
    const { save } = form;
    const switchState = (arr, state) => {
      arr.forEach((item) => {
        if (state === 'read') item.setAttribute('readonly', true);
        else if (state === 'write') item.removeAttribute('readonly');
      });
    };
    const switchBtn = (btn, state) => {
      if (state === 'read') btn.setAttribute('disabled', true);
      else if (state === 'write') btn.removeAttribute('disabled');
    };
    let editCond = 'read';
    edit.addEventListener('click', (e) => {
      e.preventDefault();
      const fields = Array.from(form.querySelectorAll('.form-input__field'));
      if (editCond === 'read') {
        edit.innerText = 'Отмена';
        editCond = 'write';
        switchState(fields, editCond);
        switchBtn(save, editCond);
      } else if (editCond === 'write') {
        edit.innerText = 'Редактировать';
        editCond = 'read';
        switchState(fields, editCond);
        switchBtn(save, editCond);
      }
    });
  }
};

const addProperty = () => {
  const prodEdit = document.forms['product-edit'];
  if (prodEdit) {
    const add = prodEdit['product-add'];
    const props = prodEdit['product-properties'];
    const btn = add.querySelector('button');
    btn.addEventListener('click', (e) => {
      e.preventDefault();
      const { propName } = add.elements;
      const id = propName.value;
      const name = propName.options[propName.selectedIndex].text;
      props.insertAdjacentHTML('afterbegin', `
        <div class="grid grid--align-center admin-product-properties__item">
          <div class="grid__cell grid__cell--5 admin-product-properties__name">
            <strong>${name}</strong>
          </div>
          <div class="grid__cell grid__cell--5 admin-product-properties__value">
            <div class="form-input form-input--text">
              <input class="form-input__field" type="text" name="${id}" value="${add.elements.propValue.value}">
            </div>
          </div>
          <div class="grid__cell grid__cell--2">
            <button class="btn btn--wide" name="delete">Удалить</button>
          </div>
        </div>
      `);
      add.elements.propValue.value = '';
    });
  }
};

const removeProperty = () => {
  const prodEdit = document.forms['product-edit'];
  if (prodEdit) {
    const props = prodEdit['product-properties'];
    props.addEventListener('click', (e) => {
      if (e.target.name === 'delete') {
        e.preventDefault();
        const item = e.target.parentElement.parentElement;
        const itemName = item.querySelector('.admin-product-properties__name').innerText;
        const forUndo = item.innerHTML;
        item.style.setProperty('height', `${item.offsetHeight}px`);
        item.innerHTML = `
        <div class="grid__cell grid__cell--5 admin-product-properties__name">
          <strong>${itemName}</strong>
        </div>
        <div class="grid__cell grid__cell--5 admin-product-properties__value">
          Удалено
        </div>
        <div class="grid__cell grid__cell--2">
          <button class="btn btn--wide" name="undo">Отменить</button>
        </div>
      `;
        const undo = item.querySelector('[name="undo"]');
        undo.addEventListener('click', (ev) => {
          ev.preventDefault();
          item.innerHTML = forUndo;
          item.style.removeProperty('height');
        });
      }
    });
  }
};

const adminProductProperties = () => {
  addProperty();
  removeProperty();
};

const cart = () => {
  const cartBlock = document.querySelector('.cart');
  const createCartItem = (item = {
    id: '', img: '', name: '', count: '', cost: '',
  }) => {
    const cartItem = document.createElement('div');
    cartItem.classList.add('cart-item');
    cartItem.dataset.id = item.id;
    const priceItem = item.count > 1 ? `1 шт. = ${item.cost} ₽` : '';
    cartItem.innerHTML = `
      <div class="cart-item__image"><img src="/images/logo.svg" alt="glyph"></div>
      <div class="cart-item__properties">
        <div class="cart-item__name">${item.name}</div>
      </div>
      <div class="cart-item__counter">
        <div class="cart-item__count-change" data-action="decrease">-</div>
        <div class="cart-item__count">${item.count}</div>
        <div class="cart-item__count-change" data-action="increase">+</div>
      </div>
      <div class="cart-item__price">
        <div class="cart-item__price-sum">${item.count * item.cost} ₽</div>
        <div>${priceItem}</div>
      </div>
      <div class="cart-item__delete"><a href=""></a></div>
    `;
    return cartItem;
  };
  const createCartSum = (summary) => {
    const cartSum = document.createElement('div');
    const input = document.querySelector('.form-login__input--price-input .form-input__field');
    input.value = summary.sum;
    cartSum.classList.add('cart-summary');
    cartSum.innerHTML = `
      <div class="cart-summary__count">${summary.count}</div>
      <div class="cart-summary__price">Итого<span>${summary.sum} ₽</span></div>
    `;
    return cartSum;
  };

  const updateSum = (sum) => {
    const sumBlock = document.querySelector('.cart-summary');
    if (sumBlock) sumBlock.remove();
    if (sum.count > 0) cartBlock.append(createCartSum(sum));
  };

  const updateForm = (list) => {
    const input = document.querySelector('.form-login__input--array-input .form-input__field');
    input.value = JSON.stringify(list);
  };

  if (cartBlock) {
    document.addEventListener('DOMContentLoaded', () => {
      if (localStorage.basket && localStorage.basket !== '{}') {
        const basket = JSON.parse(localStorage.basket);
        const bArr = Object.entries(basket);
        const summary = { count: 0, sum: 0 };
        const listIDs = [];
        cartBlock.innerHTML = '';
        bArr.forEach((item) => {
          const i = {
            id: item[0],
            img: item[1].img,
            name: item[1].name,
            count: item[1].count,
            cost: item[1].cost,
          };
          listIDs.push({ id: i.id, count: i.count });
          summary.count += i.count;
          summary.sum += i.cost * i.count;
          cartBlock.append(createCartItem(i));
        });
        summary.sum += +localStorage.delivery || 0;
        updateSum(summary);
        updateForm(listIDs);
      } else {
        cartBlock.innerHTML = '<div style="margin-top: 24px; color: #aaa">Ваша корзина пуста</div>';
        const form = document.querySelector('.form-login');
        form.remove();
      }
      const itemDel = Array.from(document.querySelectorAll('.cart-item__delete a'));
      itemDel.forEach((item) => {
        item.addEventListener('click', (e) => {
          e.preventDefault();
          const curItem = e.target.parentNode.parentNode;
          const curId = curItem.dataset.id;
          const basket = JSON.parse(localStorage.basket);
          const summary = { count: 0, sum: 0 };
          const cartIcon = document.querySelector('.header-cart__count');
          const listIDs = [];
          curItem.remove();
          delete basket[curId];
          const bArr = Object.entries(basket);
          localStorage.setItem('basket', JSON.stringify(basket));
          let cartCounter = 0;
          bArr.forEach((i) => {
            summary.count += i[1].count;
            summary.sum += i[1].cost * i[1].count;
            cartCounter += i[1].count;
            listIDs.push({ id: i[0], count: i[1].count });
          });
          summary.sum += +localStorage.delivery || 0;
          cartIcon.innerText = cartCounter !== 0 ? cartCounter : '';
          updateSum(summary);
          updateForm(listIDs);
        });
      });
      const counters = Array.from(document.querySelectorAll('.cart-item__count-change'));
      counters.forEach((counter) => {
        counter.addEventListener('click', (e) => {
          const curItem = e.target.parentNode.parentNode;
          const curId = curItem.dataset.id;
          const { action } = e.target.dataset;
          const price = curItem.querySelector('.cart-item__price-sum');
          const count = e.target.parentNode.querySelector('.cart-item__count');
          const cartIcon = document.querySelector('.header-cart__count');
          const listIDs = [];
          let c = +count.innerText;
          if (action === 'increase') c += 1;
          if (action === 'decrease') c -= 1;
          count.innerText = c;
          const basket = JSON.parse(localStorage.basket);
          basket[curId].count = c;
          price.innerText = `${basket[curId].count * basket[curId].cost} ₽`;
          if (c > 1) price.nextElementSibling.innerText = `1 шт. = ${basket[curId].cost} ₽`;
          else price.nextElementSibling.innerText = '';
          if (c === 0) {
            curItem.remove();
            delete basket[curId];
          }
          const summary = { count: 0, sum: 0 };
          const bArr = Object.entries(basket);
          localStorage.setItem('basket', JSON.stringify(basket));
          let cartCounter = 0;
          bArr.forEach((i) => {
            summary.count += i[1].count;
            summary.sum += i[1].cost * i[1].count;
            cartCounter += i[1].count;
            listIDs.push({ id: i[0], count: i[1].count });
          });
          summary.sum += +localStorage.delivery || 0;
          cartIcon.innerText = cartCounter !== 0 ? cartCounter : '';
          updateSum(summary);
          updateForm(listIDs);
        });
      });
      const delivery = Array.from(document.querySelectorAll('input[name="delivery"]'));
      delivery.forEach((del) => {
        del.addEventListener('change', () => {
          localStorage.setItem('delivery', del.dataset.price);
          const basket = JSON.parse(localStorage.basket);
          const bArr = Object.entries(basket);
          const summary = { count: 0, sum: 0 };
          const listIDs = [];
          cartBlock.innerHTML = '';
          bArr.forEach((item) => {
            const i = {
              id: item[0],
              img: item[1].img,
              name: item[1].name,
              count: item[1].count,
              cost: item[1].cost,
            };
            listIDs.push({ id: i.id, count: i.count });
            summary.count += i.count;
            summary.sum += i.cost * i.count;
            cartBlock.append(createCartItem(i));
          });
          summary.sum += +localStorage.delivery || 0;
          updateSum(summary);
          updateForm(listIDs);
        });
      });
      const form = document.forms.order;
      form.addEventListener('submit', () => {
        localStorage.removeItem('basket');
        localStorage.removeItem('delivery');
      });
    });
  }
};

const logoutCartClean = () => {
  const logoutLinks = Array.from(document.querySelectorAll('[href="/users/logout"]'));
  if (logoutLinks) {
    logoutLinks.forEach((link) => {
      link.addEventListener('click', () => {
        localStorage.removeItem('basket');
        localStorage.removeItem('delivery');
      });
    });
  }
};

cart();
sh();
productBuy();
headerCart();
formUser();
adminProductProperties();
logoutCartClean();
