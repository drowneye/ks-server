const fs = require('fs')
const excel = require('excel4node');

const statusOpt = [
  {value: 'new', name: 'Новый'},
  {value: 'active', name: 'Открыт'},
  {value: 'agreed', name: 'Согласован'},
  {value: 'denied', name: 'Отменён'},
  {value: 'shipped', name: 'Отправлен'},
  {value: 'closed', name: 'Закрыт'},
];

const payStatus = [
  {name: 'Не оплачено', value: 'false'},
  {name: 'Оплачено', value: 'true'},
];

const dateNow = new Date();

function createReport(data, res) {
  const wb = new excel.Workbook({
    defaultFont: {
      size: 12,
      name: 'Arial',
    },
    author: 'Ножики-Режики'
  });

  const options = {
    margins: { 
      bottom: 0.787402,
      left: 0.984252,
      right: 0.393701,
      top: 0.787402,
    },
  }

  const allBorder = {
    left: {
      style: 'thin',
    },
    right: {
      style: 'thin',
    },
    top: {
      style: 'thin',
    },
    bottom: {
      style: 'thin',
    },
  }
  const stHeader = wb.createStyle({
    font: {
      bold: true,
    },
    border: {
      bottom: {
        style: 'medium',
      },
    }, 
    alignment: {
      horizontal: 'right',
      vertical: 'top',
      wrapText: true,
    },
  });

  const stCell = wb.createStyle({
    border: {
      bottom: {
        style: 'thin',
      },
    }, 
    alignment: {
      horizontal: 'left',
      vertical: 'top',
      shrinkToFit: true, 
      wrapText: true,
    },
  });

  const stDate = wb.createStyle({
    border:{
      bottom: {
        style: 'thin',
      },
    },
    numberFormat: 'dd.mm.yyyy hh:mm',
    alignment: {
      horizontal: 'left',
      vertical: 'top',
      wrapText: true,
    },
  });

  const stNum = wb.createStyle({
    border: {
      bottom: {
        style: 'thin',
      },
    },
    alignment: {
      horizontal: 'right',
      vertical: 'top',
      wrapText: true,
    },
  })

  const ws = wb.addWorksheet('Лист 1', options);

  ws.column(1).setWidth(3);
  ws.column(2).setWidth(4);
  ws.column(3).setWidth(7);
  ws.column(5).setWidth(8);
  ws.column(6).setWidth(16);
  ws.cell(1,4,1,7,true)
    .string('ИП Заря Виталий Валерьевич')
    .style({
      font: {
        bold: true,
      },
      border: {
        bottom: {
          style: 'medium'
        }
      },
      alignment: {
        horizontal: 'center',
        vertical: 'center',
      },
    })
  ws.cell(2,4,2,7,true)
    .string('198264, г. Санкт-Петербург, пр. Ветеранов 171')
    .style({
      font: {
        bold: true,
      },
      alignment: {
        horizontal: 'center',
        vertical: 'center',
      },
    })
  const orgRec = [
    {bold: true},
    'ОГРН ',
    {bold: false},
    '312784704600085 ',
    {bold: true},
    'ИНН ',
    {bold: false},
    '780242068505 ',
  ]
  ws.cell(3,4,3,7,true)
    .string(orgRec)
    .style({
      alignment: {
        horizontal: 'center',
        vertical: 'center',
      },
    })


  ws.cell(5, 7, 5, 8, true)
    .date(dateNow)
    .style({
      alignment: {
        horizontal: 'center',
        vertical: 'center',
      },
      numberFormat: 'dd.mm.yyyy'
    })
  ws.cell(7, 5, 7, 6, true)
    .string('ДЕТАЛИЗАЦИЯ')
    .style({
      font: {
        bold: true,
      },
      alignment: {
        horizontal: 'center',
        vertical: 'center'
      }
    })
  ws.cell(8, 5, 8, 6, true)
    .string(`Заказ № ${data._id.toString().toUpperCase().substr(-8,8)}`)
    .style({
      alignment: {
        horizontal: 'center',
        vertical: 'center'
      }
    })

  const stColName = wb.createStyle({
    font: {
      bold: true,
    },
    border:{
      bottom: {
        style: 'thin',
      },
    },
    alignment: {
      horizontal: 'left',
      vertical: 'top',
      wrapText: true,
    },
  })

  ws.cell(10,3,10,4, true)
    .string('Дата заказа:')
    .style(stColName)
  ws.cell(11,3,11,4, true)
    .string('Заказчик: ')
    .style(stColName)
  ws.cell(12,3,12,4, true)
    .string('Телефон: ')
    .style(stColName)
  ws.cell(13,3,13,4, true)
    .string('Адрес: ')
    .style(stColName)
  ws.cell(14,3,14,4, true)
    .string('E-mail: ')
    .style(stColName) 
  ws.cell(15,3,15,4, true)
    .string('Комментарий: ')
    .style(stColName) 
  ws.cell(16,3,16,4, true)
    .string('Cтатус: ')
    .style(stColName) 
  ws.cell(17,3,17,4, true)
    .string('Оплата: ')
    .style(stColName) 
    
  const stColValue = wb.createStyle({
    border:{
      bottom: {
        style: 'thin',
      },
    },
    alignment: {
      horizontal: 'left',
      vertical: 'top',
      wrapText: true,
    },
  })
  let status = '';
  let payment = '';
  for (el of statusOpt) {
    if (el.value === data.status) status = el.name
  }
  for (el of payStatus) {
    if (el.value === data.payment) payment = el.name
  }
  ws.cell(10, 5, 10, 8, true)
    .date(data.date)
    .style(stDate)
  ws.cell(11, 5, 11, 8, true)
    .string(data.customer.name)
    .style(stColValue)
  ws.cell(12, 5, 12, 8, true)
    .string(data.customer.phone)
    .style(stColValue)
  ws.cell(13, 5, 13, 8, true)
    .string(data.customer.address)
    .style(stColValue)
  ws.cell(14, 5, 14, 8, true)
    .string(data.customer.email)
    .style(stColValue) 
  ws.cell(15, 5, 15, 8, true)
    .string(data.userNote)
    .style(stColValue)
  ws.cell(16, 5, 16, 8, true)
    .string(status)
    .style(stColValue)
  ws.cell(17, 5, 17, 8, true)
    .string(payment)
    .style(stColValue)
  
  ws.cell(19, 2)
    .string('№')
    .style(stHeader)
  ws.cell(19, 3, 19, 5, true)
    .string('Наименование')
    .style({
      font: {
        bold: true,
      },
      border: {
        bottom: {
          style: 'medium',
        },
      }, 
      alignment: {
        horizontal: 'left',
        vertical: 'top',
        wrapText: true,
      },
    })
  ws.cell(19, 6)
    .string('Цена (руб/шт)')
    .style(stHeader)
  ws.cell(19, 7)
    .string('Кол-во')
    .style(stHeader)
  ws.cell(19, 8)
    .string('Сумма (руб.)')
    .style(stHeader)

  const orders = data.order;
  for (let i = 0; i < orders.length; i++) {
    ws.cell(20+i, 2)
      .number(i+1)
      .style(stNum)
    ws.cell(20+i, 3, 20+i, 5, true)
      .string(orders[i].name)
      .style(stCell)
    ws.cell(20+i, 6)
      .number(orders[i].cost)
      .style(stNum)
    ws.cell(20+i, 7)
      .number(orders[i].count)
      .style(stNum)
    ws.cell(20+i, 8)
      .formula(`G${i+20} * F${i+20}`)
      .style(stNum)
  }

  ws.cell(20 + orders.length, 6)
    .string('Итого: ')
    .style({
      font: {
        bold: true,
      },
      alignment: {
        horizontal: 'right',
        vertical: 'center'
      },
    })
  ws.cell(20 + orders.length, 7)
    .formula(`SUM(G20:G${20 + orders.length - 1})`)
    .style({
      font: {
        bold: true,
      },
      alignment: {
        horizontal: 'right',
        vertical: 'center'
      },
      border: {
        bottom: {
          style: 'medium',
        },
      }, 
    })
  ws.cell(20 + orders.length, 8)
    .formula(`SUM(H20:H${20 + orders.length - 1})`)
    .style({
      font: {
        bold: true,
      },
      alignment: {
        horizontal: 'right',
        vertical: 'center'
      },
      border: {
        bottom: {
          style: 'medium',
        },
      }, 
    })
  
  ws.cell(20 + orders.length + 1, 5, 20 + orders.length + 1, 6, true)
    .string('Доставка: ')
    .style({
      font: {
        bold: true,
      },
      alignment: {
        horizontal: 'right',
        vertical: 'center'
      },
    })
  ws.cell(20 + orders.length + 1, 7, 20 + orders.length + 1, 8, true)
    .string(data.delivery)
    .style({
      alignment: {
        horizontal: 'right',
        vertical: 'center'
      },
      border: {
        bottom: {
          style: 'medium',
        },
      }, 
    })

  ws.cell(20 + orders.length + 2, 5, 20 + orders.length + 2, 6, true)
    .string('Итого с доставкой: ')
    .style({
      font: {
        bold: true,
      },
      alignment: {
        horizontal: 'right',
        vertical: 'center'
      },
    })
  ws.cell(20 + orders.length + 2, 7, 20 + orders.length + 2, 8, true)
    .number(data.totalCost)
    .style({
      alignment: {
        horizontal: 'right',
        vertical: 'center'
      },
      border: {
        bottom: {
          style: 'medium',
        },
      }, 
    })

  ws.cell(20 + orders.length + 4, 3, 20 + orders.length + 4, 4, true)
    .string('Заметка: ')
    .style({
      font: {
        bold: true,
      },
      alignment: {
        horizontal: 'right',
        vertical: 'top',
        
      },
    })
  ws.cell(20 + orders.length + 4, 5, 20 + orders.length + 4, 8, true)
    .string(data.managerNote)
    .style(stCell)
  
  ws.cell(20 + orders.length + 6, 3, 20 + orders.length + 6, 4, true)
    .string('Менеджер: ')
    .style({
      alignment: {
        horizontal: 'right',
        vertical: 'center',
      }
    })
  ws.cell(20 + orders.length + 6, 5, 20 + orders.length + 6, 7)
    .style({
      border: {
        bottom: { style:'medium',}
      },
    })
  const orderNum = data._id.toString().toUpperCase().substr(-8,8);
  const doc = `${orderNum}_order-details.xlsx`;
  wb.write(doc, res);
}

module.exports = {
  createReport,
}

function formatDate(date, toName) {
  let dd = date.getDate();
  if (dd < 10) dd = '0' + dd;
  let mm = date.getMonth() + 1;
  if (mm < 10) mm = '0' + mm;
  let yy = date.getFullYear();
  
  let hh = date.getHours();
  if (hh < 10) hh = '0' + hh;
  let min = date.getMinutes();
  if (min < 10) min = '0' + min;
  
  if (toName) {
    return `${dd}-${mm}-${yy}_${hh}-${min}`
  } else {
    return `${dd}.${mm}.${yy}`
  }
  
}