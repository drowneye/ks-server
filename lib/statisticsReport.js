const excel = require('excel4node');

function createReport(data, period,res) {
  const wb = new excel.Workbook({
    defaultFont: {
      size: 12,
      name: 'Arial',
    },
    author: 'Ножики-Режики'
  });
  
  const options = {
    margins: { 
      bottom: 0.787402,
      left: 0.984252,
      right: 0.393701,
      top: 0.787402,
    },
    pageSetup: {
      orientation: 'landscape'
    }
  }
  
  const allBorder = {
    left: {
      style: 'thin',
    },
    right: {
      style: 'thin',
    },
    top: {
      style: 'thin',
    },
    bottom: {
      style: 'thin',
    },
  }

  const bBorder_medium = {
    bottom: {
      style: 'medium',
    },
  }
  const bBorder = {
    bottom: {
      style: 'thin',
    },
  }
  const stHeader = wb.createStyle({
    font: {
      bold: true,
    },
    border: bBorder_medium, 
    alignment: { 
      horizontal: 'left',
      vertical: 'top',
      wrapText: true,
    },
  });

  const stHeader_right = wb.createStyle({
    font: {
      bold: true,
    },
    border: bBorder_medium, 
    alignment: { 
      horizontal: 'right',
      vertical: 'top',
      wrapText: true,
    },
  });
  
  const stCell = wb.createStyle({
    border: bBorder,
    alignment: {
      horizontal: 'left',
      vertical: 'top',
      wrapText: true,
    },
  });
  
  const stDate = wb.createStyle({
    border: allBorder,
    numberFormat: 'dd.mm.yyyy hh:mm',
    alignment: {
      horizontal: 'left',
      vertical: 'top',
      wrapText: true,
    },
  });
  
  const stNum = wb.createStyle({
    border: bBorder,
    alignment: {
      horizontal: 'right',
      vertical: 'top',
      wrapText: true,
    },
  })
  
  const ws = wb.addWorksheet('Лист 1', options);
  
  ws.column(1).setWidth(3);
  ws.cell(1,4,1,7,true)
    .string('ИП Заря Виталий Валерьевич')
    .style({
      font: {
        bold: true,
      },
      border: {
        bottom: {
          style: 'medium'
        }
      },
      alignment: {
        horizontal: 'center',
        vertical: 'center',
      },
    })
  ws.cell(2,4,2,7,true)
    .string('198264, г. Санкт-Петербург, пр. Ветеранов 171')
    .style({
      font: {
        bold: true,
      },
      alignment: {
        horizontal: 'center',
        vertical: 'center',
      },
    })
  const orgRec = [
    {bold: true},
    'ОГРН ',
    {bold: false},
    '312784704600085 ',
    {bold: true},
    'ИНН ',
    {bold: false},
    '780242068505 ',
  ]
  ws.cell(3,4,3,7,true)
    .string(orgRec)
    .style({
      alignment: {
        horizontal: 'center',
        vertical: 'center',
      },
    })

  ws.cell(7,5,7,6,true)
    .string('Статистика продаж')
    .style({
      font: {
        bold: true,
      },
      alignment: {
        horizontal: 'center',
        vertical: 'center',
      },
    })
  if (period) {
    ws.cell(8,5,8,6,true)
      .date(new Date(period))
      .style({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
        numberFormat: 'mm.yyyy',
      })
  } else {
    ws.cell(8,5,8,6,true)
      .string('За всё время')
      .style({
        alignment: {
          horizontal: 'center',
          vertical: 'center',
        },
      })
  }
  
  ws.column(2).setWidth(4);
  ws.column(6).setWidth(17);
  ws.column(7).setWidth(15);
  ws.column(8).setWidth(8);
  ws.cell(11,2)
    .string('№')
    .style(stHeader)
  ws.cell(11, 3, 11, 5, true)
    .string('Наименование товара')
    .style(stHeader)
  ws.cell(11,6)
    .string('Стоимость (руб.)')
    .style(stHeader_right)
  ws.cell(11,7,)
    .string('Продано (шт.)')
    .style(stHeader_right)
  ws.cell(11,8,11,9,true)
    .string('Сумма (руб.)')
    .style(stHeader_right)

  for (let i = 0; i < data.length; i++) {
    ws.cell(i+12,2)
      .number(i+1)
      .style(stNum)
    ws.cell(i+12, 3, i+12, 5, true)
      .string(data[i].name)
      .style(stCell)
    ws.cell(i+12, 6)
      .number(data[i].cost)
      .style(stNum)
    ws.cell(i+12,7,)
      .number(data[i].count)
      .style(stNum)
    ws.cell(i+12,8,i+12,9,true)
      .formula(`F${i+12}*G${i+12}`)
      .style(stNum)
  }
  ws.cell(12 + data.length, 6)
    .string('Итого: ')
    .style({
      font: {
        bold: true,
      },
      alignment: {
        horizontal: 'right',
        vertical: 'center',
      },
    })
  ws.cell(12 + data.length, 7)
    .formula(`SUM(G12:G${11 + data.length})`)
    .style({
      font: {
        bold: true,
      },
      alignment: {
        horizontal: 'right',
        vertical: 'center',
      },
      border: bBorder_medium,
    })
  ws.cell(12 + data.length, 8,12 + data.length, 9, true)
    .formula(`SUM(H12:I${11 + data.length})`)
    .style({
      font: {
        bold: true,
      },
      alignment: {
        horizontal: 'right',
        vertical: 'center',
      },
      border: bBorder_medium,
    })
  ws.cell(15 + data.length, 4, 15 + data.length, 5, true)
    .string('Менеджер: ')
    .style({
      font: {
        bold: true,
      },
      alignment: {
        horizontal: 'right',
        vertical: 'center',
      },
    })
  ws.cell(15 + data.length, 6, 15 + data.length, 9, true)
    .style({
      border: bBorder_medium,
    })

  wb.write(`statistics-${period}.xlsx`, res);
}

module.exports = { createReport };