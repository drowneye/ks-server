const User = require('../models/user');
async function getFilteredUsers(query, groups, array) {
  let byGroup = false;
  let byText = false;
  let users = array;
  try {
    if(query.byGroup && query.byGroup !== '') {
      let qr;
      for (g of groups) {
        if(g.name === query.byGroup) qr = g.value;
      } 
      byGroup = await User.find({group: qr})
      console.log(byGroup);
    }

    if(query.search && query.search !== '') {
      const qr = query.search;
      byText = await User.find({name: {$regex: qr}})
      console.log(byText);
    }

    const filtered = [byText, byGroup].filter((item) => Array.isArray(item));
    filtered.forEach((arr) => users = users.filter((i) => arr.includes(i)));
    return users;
  } catch (error) {
    console.log('Ошибка фильтрации данных Пользователей.');
    console.error(error);
  }
  

}

module.exports = {getFilteredUsers};