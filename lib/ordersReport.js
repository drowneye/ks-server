const excel = require('excel4node');
const dateNow = new Date();

function createReport(data, res, period) {

const wb = new excel.Workbook({
  defaultFont: {
    size: 12,
    name: 'Arial',
  },
  author: 'Ножики-Режики'
});

const options = {
  margins: { 
    bottom: 0.787402,
    left: 0.984252,
    right: 0.393701,
    top: 0.787402,
  },
  pageSetup: {
    orientation: 'landscape'
  }
}

const allBorder = {
  left: {
    style: 'thin',
  },
  right: {
    style: 'thin',
  },
  top: {
    style: 'thin',
  },
  bottom: {
    style: 'thin',
  },
}
const stHeader = wb.createStyle({
  font: {
    bold: true,
  },
  border: allBorder, 
  alignment: {
    horizontal: 'center',
    vertical: 'top',
    wrapText: true,
  },
  
});

const stCell = wb.createStyle({
  border: allBorder,
  alignment: {
    horizontal: 'left',
    vertical: 'top',
    wrapText: true,
  },
});

const stDate = wb.createStyle({
  border: allBorder,
  numberFormat: 'dd.mm.yyyy hh:mm',
  alignment: {
    horizontal: 'left',
    vertical: 'top',
    wrapText: true,
  },
});

const stNum = wb.createStyle({
  border: allBorder,
  alignment: {
    horizontal: 'right',
    vertical: 'top',
    wrapText: true,
  },
})

const ws = wb.addWorksheet('Лист 1', options);

ws.column(1).setWidth(3);
ws.cell(1,4,1,7,true)
  .string('ИП Заря Виталий Валерьевич')
  .style({
    font: {
      bold: true,
    },
    border: {
      bottom: {
        style: 'medium'
      }
    },
    alignment: {
      horizontal: 'center',
      vertical: 'center',
    },
  })
ws.cell(2,4,2,7,true)
  .string('198264, г. Санкт-Петербург, пр. Ветеранов 171')
  .style({
    font: {
      bold: true,
    },
    alignment: {
      horizontal: 'center',
      vertical: 'center',
    },
  })
const orgRec = [
  {bold: true},
  'ОГРН ',
  {bold: false},
  '312784704600085 ',
  {bold: true},
  'ИНН ',
  {bold: false},
  '780242068505 ',
]
ws.cell(3,4,3,7,true)
  .string(orgRec)
  .style({
    alignment: {
      horizontal: 'center',
      vertical: 'center',
    },
  })


    

  ws.cell(6,4,6,7,true)
    .string('ЛИСТ ЗАКАЗА')
    .style({
      font: {
        bold: true,
      },
      alignment: {
        horizontal: 'center',
        vertical: 'center',
      },
    })
  ws.cell(5,9)
    .date(dateNow)
    .style({
      alignment: {
        horizontal: 'center',
        vertical: 'center',
      },
      numberFormat: 'dd.mm.yyyy'
    })

    if (period) {
      ws.cell(7,4,7,5, true)
        .string('Отчетный период: ')
        .style({
          alignment: {
            horizontal: 'left',
            vertical: 'center',
          },
        })
      ws.cell(7,6,7,7, true)
        .date(new Date(period))
        .style({
          alignment: {
            horizontal: 'center',
            vertical: 'center',
          },
          numberFormat: 'mm.yyyy',
        })
    } 
  
  ws.cell(10, 2)
    .string('№')
    .style(stHeader)
  ws.column(3).setWidth(15);
  ws.cell(10, 3)
    .string('Дата')
    .style(stHeader)
  ws.cell(10, 4, 10, 5, true)
    .string('Заказчик')
    .style(stHeader)
  ws.cell(10, 6)
    .string('Статус')
    .style(stHeader)
  ws.cell(10, 7)
    .string('Доставка')
    .style(stHeader)
  ws.cell(10, 8)
    .string('Позиций (шт)')
    .style(stHeader)
  ws.cell(10, 9)
    .string('Сумма заказа (руб.)')
    .style(stHeader)

  for (let i = 0; i < data.length; i++) {
    ws.cell(11 + i, 2)
      .string(data[i].orderNum)
      .style(stCell)
    ws.cell(11 + i, 3)
      .date(new Date(data[i].ISODate))
      .style(stDate)
    ws.cell(11 + i, 4, 11 + i, 5, true)
      .string(data[i].customerName)
      .style(stCell)
    ws.cell(11 + i, 6)
      .string(data[i].status)
      .style(stCell)
    ws.cell(11 + i, 7)
      .string(data[i].delivery)
      .style(stCell)
    ws.cell(11 + i, 8)
      .number(data[i].totalCount)
      .style(stNum)
    ws.cell(11 + i, 9)
      .number(data[i].totalCost)
      .style(stNum)
  }
  ws.cell(11 + data.length, 7)
    .string('Итого')
    .style(stHeader)
  ws.cell(11 + data.length, 8)
    .formula(`SUM(H11:H${11+data.length-1})`)
    .style(stNum)
  ws.cell(11 + data.length, 9)
    .formula(`SUM(I11:I${11+data.length-1})`)
    .style(stNum)

  ws.cell(11+data.length+3, 4, 11+data.length+3, 6, true)
    .string('Индивидульный предприниматель:')
    .style({
      alignment: {
        horizontal: 'right',
        vertical: 'center'
      }
    });
  ws.cell(11+data.length+3, 8)
    .style({
      border: {
        bottom: {
          style: 'thin'
        },
      },
    })
  ws.cell(11+data.length+3, 9)
    .string('Заря В.В.')
    .style({
      font: {
        bold: true
      },
      alignment: {
        horizontal: 'left',
        vertical: 'center'
      }
    })

  ws.cell(11+data.length+5, 4, 11+data.length+5, 6, true)
    .string('Менеджер:')
    .style({
      alignment: {
        horizontal: 'right',
        vertical: 'center'
      }
    });
  ws.cell(11+data.length+5, 8,11+data.length+5,9)
    .style({
      border: {
        bottom: {
          style: 'thin'
        },
      },
    })
  
  wb.write(`orders-${formatDate(dateNow, true)}.xlsx`, res);
}

function formatDate(date, toName) {
  let dd = date.getDate();
  if (dd < 10) dd = '0' + dd;
  let mm = date.getMonth() + 1;
  if (mm < 10) mm = '0' + mm;
  let yy = date.getFullYear();
  
  let hh = date.getHours();
  if (hh < 10) hh = '0' + hh;
  let min = date.getMinutes();
  if (min < 10) min = '0' + min;
  
  if (toName) {
    return `${dd}-${mm}-${yy}_${hh}-${min}`
  } else {
    return `${dd}.${mm}.${yy}`
  }
  
}

module.exports = {
  createReport,
};