const payStatus = {
  false: 'Не оплачено',
  true: 'Оплачено',
}

const statusOpt = {
  new: 'Новый',
  active: 'Открыт',
  agreed: 'Согласован',
  denied: 'Отменён', 
  shipped: 'Отправлен',
  closed:'Закрыт', 
} 

function orderFilter(products, query) {
  let dateFilter = false;
  let statusFilter = false;
  let paymentFilter = false;
  let orders = products;

  if (query['start-date'] && query['end-date']){
    const dateStart = Date.parse(query['start-date']);
    const dateEnd = Date.parse(query['end-date']);
    let localArr = [];
    for (item of products) {
      const orderDate = Date.parse(item.ISODate)
      if ((orderDate >= dateStart) && (orderDate <= dateEnd)) {
        localArr.push(item);
      }
    }
    if (localArr.length > 0) {
      dateFilter = localArr;
    }
  }

  if(query.status) {
    const statusQuery = query.status;
    let localArr = [];
    for (item of products) {
      if(item.status === statusOpt[statusQuery]) {
        localArr.push(item);
      }
    }
    if (localArr.length > 0) {
      statusFilter = localArr;
    }
  } 

  if(query.payment) {
    const paymentQuery = query.payment;
    let localArr = [];
    for (item of products) {
      if(item.payment === payStatus[paymentQuery]) {
        localArr.push(item);
      }
    }
    if (localArr.length > 0) {
      paymentFilter = localArr;
    }
  }

  const filtered = [dateFilter, statusFilter, paymentFilter].filter((item) => Array.isArray(item));
  filtered.forEach((arr) => orders = orders.filter((i) => arr.includes(i)));
  return orders;

}

function getFilterToPrint(orders, query) {
  let filteredOrders = orders;
  let dateFilter = false;
  let statusFilter = false;
  
  if (query.period){
    const dateStart = new Date(query.period)
    const dateEnd = new Date(query.period)
    dateEnd.setMonth(dateEnd.getMonth() + 1);
    let localArr = [];
    for (item of orders) {
      const orderDate = Date.parse(item.ISODate)
      if ((orderDate >= Date.parse(dateStart)) && (orderDate < Date.parse(dateEnd))) {
        localArr.push(item);
      }
    }
    if (localArr.length > 0) {
      dateFilter = localArr;
    }
  }

  if(query.status) {
    const statusQuery = query.status;
    let localArr = [];
    for (item of orders) {
      if(item.status === statusOpt[statusQuery]) {
        localArr.push(item);
      }
    }
    if (localArr.length > 0) {
      statusFilter = localArr;
    }
  }

  const filtered = [dateFilter, statusFilter].filter((item) => Array.isArray(item));
  filtered.forEach((arr) => filteredOrders = filteredOrders.filter((i) => arr.includes(i)));
  return filteredOrders;
}

module.exports = {orderFilter, getFilterToPrint};
