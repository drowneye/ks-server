const sharp = require('sharp');
const path = require('path');
const mime = require('mime');

resize3x = (file) => {
  const newPath = './public/images/sharped/'
  const ext = path.extname(file.filename);
  const name = extractName(file, ext)
  let img = {
    name: name,
    formats: [],
    density: [2, 3],
  }
  resize2x(file); // Вызов обработки 2го уровня плотности
 
  sharp(file.path)
    .resize({ width: 552*3 })
    .toFile(`${newPath}${name}@3x${ext}`)
    .then(info => { console.log(`uploaded: ${newPath}${name}@3x${ext}`) })
    .catch(err => { console.error(err) });
  let extMime = mime.getType(`${newPath}${name}${ext}`);
  let format = {
    ext: ext,
    mime: extMime,
    orig: true,
  }
  img.formats.push(format)

  if(ext !== '.webp') {
    sharp(file.path)
      .resize({ width: 552*3 })
      .webp()
      .toFile(`${newPath}${name}@3x.webp`)
      .then(info => { console.log(`uploaded: ${newPath}${name}@3x.webp`) })
      .catch(err => { console.error(err) });
    let extMime = mime.getType(`${newPath}${name}.webp`);
    let format = {
      ext: '.webp',
      mime: extMime,
    }
    img.formats.push(format)
  }

  return img;
};

resize2x = (file) => {
  const newPath = './public/images/sharped/';
  const ext = path.extname(file.filename);
  const name = extractName(file, ext);
  let img = {
    name: name,
    formats: [],
    density: [2],
  }
  resize1x(file); // Вызов обработки 1го уровня плотности
  

  sharp(file.path)
    .resize({ width: 552*2 })
    .toFile(`${newPath}${name}@2x${ext}`)
    .then(info => { console.log(`uploaded: ${newPath}${name}@2x${ext}`) })
    .catch(err => { console.error(err) });
  let extMime = mime.getType(`${newPath}${name}${ext}`);
  let format = {
    ext: ext,
    mime: extMime,
    orig: true,
  }
  img.formats.push(format)

  if(ext !== '.webp') {
    sharp(file.path)
      .resize({ width: 552*2 })
      .webp()
      .toFile(`${newPath}${name}@2x.webp`)
      .then(info => { console.log(`uploaded: ${newPath}${name}@2x.webp`) })
      .catch(err => { console.error(err) });
    let extMime = mime.getType(`${newPath}${name}.webp`);
    let format = {
      ext: '.webp',
      mime: extMime,
    }
    img.formats.push(format)
  }

  return img;
  
};

resize1x = (file) => {
  const newPath = './public/images/sharped/';
  const ext = path.extname(file.filename);
  const name = extractName(file, ext);
  let img = {
    name: name,
    formats: [],
    density: [],
  }

  sharp(file.path)
    .resize({ width: 552 })
    .toFile(`${newPath}${name}${ext}`)
    .then(info => { console.log(`uploaded: ${newPath}${name}${ext}`) })
    .catch(err => { console.error(err) });
  let extMime = mime.getType(`${newPath}${name}${ext}`);
  let format = {
    ext: ext,
    mime: extMime,
    orig: true,
  }
  img.formats.push(format)

  if(ext !== '.webp') {
    sharp(file.path)
      .resize({ width: 552 })
      .webp()
      .toFile(`${newPath}${name}.webp`)
      .then(info => { console.log(`uploaded: ${newPath}${name}.webp`) })
      .catch(err => { console.error(err) });
    let extMime = mime.getType(`${newPath}${name}.webp`);
    let format = {
      ext: '.webp',
      mime: extMime,
    }
    img.formats.push(format)
  
  }
  
  return img;
};

resizeNone = (file) => {
  const newPath = './public/images/sharped/';
  const ext = path.extname(file.filename);
  const name = extractName(file, ext);
  let img = {
    name: name,
    formats: [],
    density: [],
  }
  

  sharp(file.path)
    .toFile(`${newPath}${name}${ext}`)
    .then(info => { console.log(`uploaded: ${newPath}${name}${ext}`) })
    .catch(err => { console.error(err) });
  let extMime = mime.getType(`${newPath}${name}${ext}`);
  let format = {
    ext: ext,
    mime: extMime,
    orig: true,
  }
  img.formats.push(format)
  
  if(ext !== '.webp') {
    sharp(file.path)
      .webp()
      .toFile(`${newPath}${name}.webp`)
      .then(info => { console.log(`uploaded: ${newPath}${name}.webp`) })
      .catch(err => { console.error(err) });
    let extMime = mime.getType(`${newPath}${name}.webp`);
    let format = {
      ext: '.webp',
      mime: extMime,
    }
    img.formats.push(format)
  }
  
  return img;
};

extractName = (file, ext) => {
    const name = file.filename;
    if (name.includes(ext)) {
        return name.slice(0, name.indexOf(ext))
    } else {
        return name;
    }
}

module.exports = {
    resize3x, resize2x, resize1x, resizeNone,
} 