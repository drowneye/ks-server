const SefUrl = require('../../models/sefurl');
const Product = require('../../models/product');
const Order = require('../../models/order');
const User = require('../../models/user');
const Delivery = require('../../models/delivery');

async function getOrderDetail(id) {
  try {
    const currentOrder = await Order.findById(id);
    const customer = await User.findById(currentOrder.userId, {hash: 0, salt: 0});
    const delivery = await Delivery.findById(currentOrder.delivery);
    let orderList = [];
    let totalCost = 0;
    for (item of currentOrder.orderlist) {
      let prod = await Product.findById(item.id);
      let href = await SefUrl.findOne({elementId: item.id});
      orderList.push({
        name: prod.name,
        count: item.count,
        cost: prod.cost,
        href: href.link,
        img: prod.img,
      });
      totalCost += item.count * prod.cost;
    }
    const orderDetail = {
      _id: currentOrder._id,
      order: orderList,
      totalCost: totalCost + currentOrder.deliveryCost,
      customer: customer,
      userNote: currentOrder.userComment,
      managerNote: currentOrder.managerComment,
      delivery: `${delivery.name} +${currentOrder.deliveryCost} ₽`,
      status: currentOrder.status,
      payment: currentOrder.payment,
      date: currentOrder.date,
    }
    return orderDetail;
  } catch (error) {
    console.log(`ОШИБКА: некорректный запрос по заказу ${id}`);
    console.error(error);
  }
}

module.exports = {
  getOrderDetail,
}