const Product = require('../../models/product');
const User = require('../../models/user');
const Delivery = require('../../models/delivery');

const statusOpt = [
  {value: 'new', name: 'Новый'},
  {value: 'active', name: 'Открыт'},
  {value: 'agreed', name: 'Согласован'},
  {value: 'denied', name: 'Отменён'},
  {value: 'shipped', name: 'Отправлен'},
  {value: 'closed', name: 'Закрыт'},
];

const payStatus = [
  {name: 'Не оплачено', value: 'false'},
  {name: 'Оплачено', value: 'true'},
];

async function getOrderList(orders) {
  let orderList = [];
  try {
    for (item of orders) {
      const customerName = await User.findById(item.userId, {name: 1});
      const delivery = await Delivery.findById(item.delivery);
      let totalCount = 0;
      let totalCost = 0;
      let tStatus = '';
      let tPayment = '';
      for (productId of item.orderlist) {
        prod = await Product.findById(productId.id);
        totalCount += productId.count;
        totalCost += productId.count * prod.cost;
      }
      for (el of statusOpt) {
        if (el.value === item.status) tStatus = el.name
      }
      for (el of payStatus) {
        if (el.value === item.payment) tPayment = el.name
      }
      orderList.push({
        id: item._id,
        orderNum: item._id.toString().toUpperCase().substr(-8,8),
        customerName: customerName.name,
        payment: tPayment,
        date: formatDate(item.date),
        ISODate: item.date,
        status: tStatus,
        delivery: delivery.name,
        totalCost,
        totalCount,
      })
    }
    return orderList;
  } catch (error) {
    console.error(error)
  }
}

module.exports = {
  getOrderList,
}

function formatDate(date) {
  let dd = date.getDate();
  if (dd < 10) dd = '0' + dd;
  let mm = date.getMonth() + 1;
  if (mm < 10) mm = '0' + mm;
  let yy = date.getFullYear();

  let hh = date.getHours();
  if (hh < 10) hh = '0' + hh;
  let min = date.getMinutes();
  if (min < 10) min = '0' + min;
  
  return {
    date: `${dd}.${mm}.${yy}`,
    time: `${hh}:${min}`
  }
}