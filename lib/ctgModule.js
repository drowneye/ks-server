const { constants } = require('crypto');
const Category = require('../models/category');
const Product = require('../models/product');
const SefUrl = require('../models/sefurl');

async function getChildList(idCtg){
    try {
        let childList = await Category.find({parent: idCtg});
        return childList;
    } catch(error) {
        console.log('function: getChilList --- ERROR');
        console.error(error);
    }
}

//функция автоматически обновляет уровень вложенности у дочерних элементов указанного массива элементов
async function updateLevel(element){
    let childList = await getChildList(element);
    let newLevel = {"_id": "", "level": 0};
    if(childList.length == 0) {
        console.log(`function: updateLevel --- ${element} Нет дочерних элементов.`);
    } else {
        for (item of childList) { 
            try {
                if(item.parent) {
                    newLevel = await Category.findOne({_id: item.parent}, {level: 1});
                }
                await Category.findOneAndUpdate({_id: item._id}, {level: newLevel.level + 1});
                await updateLevel(item._id); //Запуск рекурсии.
                //console.log(`function: updateLevel --- ${item._id} ${item.name} - level is updated`);
            } catch(error) {
                console.log(`function: updateLevel --- ERROR on ${item._id}`)
                console.error(error)
            }
        }
    }
}

/* Функция возвращает массив категорий, отсортированный в иерархическом порядке */
async function getSortedCategory(inputArr, outputArr) {
    if(inputArr === null || undefined) {
        console.log('Нет дочерних элементов.');
    } else {
        for (elem of inputArr) {
            outputArr.push(elem) 
            await getSortedCategory(await getChildList(elem._id), outputArr);
        }
    }   
    return outputArr;
}


/* 
    Функция возврата значений ссылки и родителя текущего элемента.
    Определяет тип элемента: категория или продукт. Исходя из этого отправляет запрос к БД.
*/
async function getParentData(id) {
    try {
        return await Category.findById(id);
    } catch(error) {
        console.log('function: getParentData --- ERROR');
        console.log(error);
    }
} 

/*  
    Функция собирает весь url от начального элемента. 
    Рекурсия: вызывает сама себя до тех пор, пока у проверяемого элемента есть родитель.
*/
async function getFullUrl(id) {
    try {
        let parentData = await Category.findById(id);
        if (parentData.parent === '') {
            return `/${parentData.href}`;
        } else {
            return await getFullUrl(parentData.parent) + `/${parentData.href}`;
        }    
    } catch (error) {
        console.log('function: getFullUrl --- ERROR');
        console.error(error)
    }
    
}

/* 
Функция проверки повторения имен в базе. 
При нахождениии дублирования предупреждает полльзователя о дублировании
Возможно стоит вынести и для функции "добавить"
*/
async function checkDoubleNames(checkName) {
    let checker = await Category.find({'name': checkName}).catch((err) => {throw err});
    if (checker) {
        return true;
    } else {
        return false;
    }
} 

// функция проверяет наличие дубликатов при записи новой ссылки, игнорируя текущую запись
async function checkForDoubles(item, adress) {
    let doubles = 0;
    try {
        let dbCheck = await SefUrl.find({link: adress});
        if (dbCheck.length > 0) {
            dbCheck.forEach((hru) => {
                doubles++;
                if (hru.elementId === item) { doubles--; };
            });
            if (doubles > 0) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }    
    } catch (error) {
        console.log('function: checkForDoubles --- ERROR');
        console.error(error);
    }
     
}
// функция находит дубликаты при перезаписи дочерних элементов
async function findDoubles(itemId, adress) {
    let doubles = [];
    try {
        let dbCheck = await SefUrl.find({link: adress});
        if (dbCheck) {
            for (hru of dbCheck) {
                if (hru.elementId === itemId) {
                    console.log('Текущий элемент')
                } else {
                    doubles.push(hru)
                }
            }
            if (doubles.length == 0 ) {
                return null;
            } else {
                return doubles;
            }
        } else {
            return null;
        }    
    } catch (error) {
        console.log('function -------> findDoubles');
        console.error(error);
    }
    
}
// функция обновляет ссылки у дочерних элементов
async function updateChildURL(element) {
    let childList = await getChildList(element);
    if (childList.length === 0) {
        console.log(`function: updateChilUrl --- ${element} не имеет дочених элементов.`);
    } else {
        try {
            for (item of childList) {
                let adress = await getFullUrl(item._id);
                console.log(`function: updateChilUrl --- Элементу ${item._id} будет присвоен URL: ${adress}`);   
                await SefUrl.findOneAndUpdate({elementId: item._id}, {link: adress});
                await updateChildURL(item._id);
           }    
        } catch (error) {
            console.log(`function: updateChildUrl --- ERROR on ${element}`);
            console.error(error);
        }
    }
}

async function updateOnDelete(id) {
    let childList = await getChildList(id);
    if(childList.length == 0) {
        console.log(`function: updateOnDelete --- ${id} не имеет дочених элементов.`);
    } else {
        try {
            let element = await Category.findById(id);
            let parentUpdate = element.parent ? element.parent : '';
            for (item of childList) {
                console.log(`===== Reading ${item.name} : ${item._id} ======`)
                await Category.findOneAndUpdate({_id: item._id}, {parent: parentUpdate, level: item.level - 1});
                let adress = await getFullUrl(item._id);
                await SefUrl.findOneAndUpdate({elementId: item._id}, {link: adress});
                await updateLevel(item._id);
            }
        } catch (error) {
            console.log('function: updateOnDelete --- ERROR');
            console.error(error);
        }

        try {
            for (item of childList) {
                console.log(`===== updating SEF URL ${item.name} : ${item._id} ======`);
                await updateChildURL(item._id);
            }
        } catch (error) {
            console.error(error);
        }
    }
}

async function updateProductsURL(id) {
  try {
    const products = await Product.find({category: id});
    for (prod of products) {
      const ctgLink = await SefUrl.findOne({elementId: prod.category});
      await SefUrl.findOneAndUpdate({elementId: prod._id}, {link: `${ctgLink.link}/${prod.translit}`});
      console.log(`${prod._id} - ссылка успешно обновлена`)
    }
  } catch (error) {
    console.log('function: updateProductsURL --- ERROR');
    console.error(error);
  }
}

async function updateProdOnDelete(id) {

  try {
    const ctg = await Category.findById(id);
    const products = await Product.find({category: id});
    if (ctg.parent || ctg.parent !== '') {
      for (prod of products) {
        await Product.findByIdAndUpdate(prod._id, {category: ctg.parent});
        await updateProductsURL(ctg.parent);
        console.log(`${prod._id} - parent exists`);
      }
    } else {
      for (prod of products) {
        await Product.findByIdAndUpdate(prod._id, {category: ''});
        await SefUrl.findOneAndUpdate({elementId: prod._id}, {link: `/${prod.translit}`});
        console.log(`${prod._id} - parent not exists`);
      }
    }
    
  } catch (error) {
    console.log('function: updateProdOnDelete --- ERROR');
    console.error(error);
  }
}

module.exports = {
    getSortedCategory,
    getFullUrl,
    updateLevel,
    checkForDoubles,
    updateChildURL,
    updateOnDelete,
    updateProductsURL,
    updateProdOnDelete,
};