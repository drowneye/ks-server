const Product = require('../models/product');
const Order = require('../models/order');

async function getStatistics(period) {
  try {
    let orders = await Order.find({status: {$nin: ['new', 'denied' ]}, payment: 'true'}); // status != new && denied, payment = true
  
    if(period) {
      const dateStart = new Date(period);
      const dateEnd = new Date(period)
      dateEnd.setMonth(dateEnd.getMonth()+1);
      console.log(`${dateStart} / ${dateEnd}`);
      let localArr = [];
      for (item of orders) {
        const orderDate = Date.parse(item.date)
        if ((orderDate >= Date.parse(dateStart)) && (orderDate < Date.parse(dateEnd))) {
          localArr.push(item);
        }
        if (localArr.length > 0) {
          orders = localArr;
        }
      }
    } else {
      console.log('Период не указан. Сборка по всем заказам.');
    }
    
    let products = await Product.find({}, {_id: 1, name: 1, cost: 1})
    let counted = [];
    for (prod of products) {
      let obj = {
        id: prod._id,
        name: prod.name,
        cost: prod.cost,
        count: 0,
      };
      for (order of orders) {
        for( item of order.orderlist) {
          if(prod._id.toString() === item.id) {
            obj.count += item.count;
          }
        }
      }
      counted.push(obj);
    }
    
    return counted; 
  } catch (error) {
    console.log('Ошибка при сборе статистики');
    console.error(error);
  }
  
  //console.log(orders);
}

module.exports = { getStatistics, }