const { request } = require('express');
const multer = require('multer');
const path = require('path');

let storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/uploads/');
  },
  filename: function (req, file, cb) {
    const date = Date.now().toString(16);
    cb(null, date + path.extname(file.originalname));
  }
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/png' || file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/webp') {
    cb(null, true);
  } else {
    cb(null, false);
  }
}

function formatDate(date) {

  let dd = date.getDate();
  if (dd < 10) dd = '0' + dd;

  let mm = date.getMonth() + 1;
  if (mm < 10) mm = '0' + mm;

  let yy = date.getFullYear();

  let hh = date.getHours();
  if (hh < 10) hh = '0' + hh;

  let min = date.getMinutes();
  if (min < 10) min = '0' + min;

  return `${dd}${mm}${yy}-${hh}${min}`;
}

module.exports = multer({
  storage: storage,
  fileFilter: fileFilter,
})

