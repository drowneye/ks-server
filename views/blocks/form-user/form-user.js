export default function formUser() {
  const form = document.forms['form-user'];
  if (form) {
    const { edit } = form;
    const switchState = (arr, state) => {
      arr.forEach((item) => {
        if (state === 'read') item.setAttribute('readonly', true);
        else if (state === 'write') item.removeAttribute('readonly');
      });
    };
    let editCond = 'read';
    edit.addEventListener('click', (e) => {
      e.preventDefault();
      const fields = Array.from(form.querySelectorAll('.form-input__field'));
      if (editCond === 'read') {
        edit.innerText = 'Отмена';
        editCond = 'write';
        switchState(fields, editCond);
      } else if (editCond === 'write') {
        edit.innerText = 'Редактировать';
        editCond = 'read';
        switchState(fields, editCond);
      }
    });
  }
}
