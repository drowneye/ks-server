export default function productBuy() {
  const buyBtn = Array.from(document.querySelectorAll('.product-price__btn a'));
  const cart = document.querySelector('.header-cart__count');
  if (localStorage.basket) localStorage.setItem('basket', JSON.stringify({}));
  buyBtn.forEach((btn) => {
    btn.addEventListener('click', (e) => {
      e.preventDefault();
      const basket = JSON.parse(localStorage.basket);
      const pId = e.target.dataset.id;
      basket[pId] = basket[pId] ? basket[pId] += 1 : 1;
      const bArr = Object.values(basket);
      // console.log(basket, bArr);
      let cartCounter = 0;
      bArr.forEach((item) => {
        cartCounter += item;
      });
      cart.innerText = cartCounter;
      localStorage.setItem('basket', JSON.stringify(basket));
    });
  });
}
