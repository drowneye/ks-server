const {Schema, model} = require('mongoose');

const userSchema = new Schema({
    name : {
        type: String,
        required: true
    },
    email : {
        type: String,
        required: true
    },
    phone : {
        type: String,
       
    }, 
    address: {
        type: String
    }, 
    hash: String,
    salt: String,
    group: {
      type: String,
      default: 'user'
    },
    regdate: {
        type: Date,
        default: new Date(),
    },
});

const User = module.exports = model('User', userSchema);