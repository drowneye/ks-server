const {Schema, model} = require('mongoose');

const orderSchema = new Schema({
    userId: {
        type: String,
    },
    orderlist: [Object],
    price: {
        type: Number,
        required: true
    },
    delivery: {
        type: String,
        default: 'Самовывоз'
    },
    deliveryCost: {
      type: Number
    },
    date: {
        type: Date,
        default: new Date()
    }, 
    status: {
        type: String,
        default: 'new'
    },
    payment: {
        type: String,
        default: 'false',
    },
    userComment : {
        type: String
    },
    managerComment : {
        type: String
    },
});

const Order = module.exports = model('Order', orderSchema);