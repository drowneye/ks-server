const {Schema, model} = require('mongoose');
// ЧПУ Роутинг
const sefUrlSchema = new Schema({
    elementId: {
        type: String,
        required: true
    },
    link: {
       type: String,
       required: true
    },
    type: {
        type: String,
        required: true
    },
});

const Sefurl = module.exports = model('Sefurl', sefUrlSchema);
