const {Schema, model} = require('mongoose');

//    Category Schema
const catSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    href: {
        type: String,
        required: true
    },
    parent: String,  //в парент записывается id родительской категории
    level: Number,   //Уровень вложенности категории    
})

const Category = module.exports = model('Category', catSchema);
