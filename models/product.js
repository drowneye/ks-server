const {Schema, model} = require('mongoose');

//Article Schema
const productSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    translit: {
        type: String,
        required: true
    },
    desc: {
        type: String,
        required: true
    },
    category: {
       type: String,
       required: true
    },
    img: {
      type: Object,
    },
    cost: {
        type: Number,
        required: true
    },
    ts: {
        type: Date,
        default: new Date()
    },
    hidden: {
        type: Boolean,
        default: true,
    },
    params: [Object],
 });
productSchema.index({'$**': 'text'});
const Product = module.exports = model('Product', productSchema);