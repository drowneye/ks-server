const {Schema, model} = require('mongoose');

const propertySchema = new Schema({
  name: {
    type: String,
    required: true,
  },
});

const Property = module.exports = model('Property', propertySchema);