const {Schema, model} = require('mongoose')

const deliverySchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  cost: {
    type: Number,
    required: true,
  },
});

const Delivery = module.exports = model('Delivery', deliverySchema);
