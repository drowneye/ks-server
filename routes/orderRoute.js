const {Router} = require('express');
const router = Router();

const User = require('../models/user');
const Order = require('../models/order');
const Product = require('../models/product');
const Delivery = require('../models/delivery');
const Mailer = require('../lib/mailer');
const pug = require('pug');
router.get('/cart', async (req, res, next) => {
  let currentUser = {};
  let deliveries = [];
  try {
    if (global.user) {
      let id = global.user._id;
      currentUser = await User.findById(id);
    }
    deliveries = await Delivery.find()
    
  } catch (error) {
    console.error(error)
  }
  
  res.render('cart', {
    title: 'Корзина',
    crUser: currentUser || 'андеф',
    deliveries: deliveries,
  });
});

router.post('/cart', async (req, res, next) => {
  try {
    const name = req.body.name;
    const email = req.body.email;
    const phone = req.body.phone;
    const address = req.body.address;
    const comment = req.body.clarifications;
    const delivery = req.body.delivery;
    const arr = JSON.parse(req.body.products);
    const price = req.body.price;
    const deliveryReq = await Delivery.findById(delivery);  

    const client = {
      name,
      email,
      phone,
      address,
    } 

    let orders = []
    for (el of arr) {
      let prod = await Product.findById(el.id, {name: 1, cost: 1});
      let item = {
        id: prod._id,
        name: prod.name,
        cost: prod.cost,
        count: el.count,
        sum: prod.cost * el.count,
      }
      orders.push(item);
    }
    

    const newOrder = new Order({
      userId: global.user._id || '',
      orderlist: arr,
      price: price,
      delivery: delivery,
      deliveryCost: Number(deliveryReq.cost),
      userComment: comment,
    })
    
    newOrder.save()
      .then((order) => {
          console.log(`Заказ ${order._id} успешно оформлен.`);
      }); 

    let letter = pug.renderFile('./views/mailTmpl.pug', {orders});
    Mailer.orderMail(letter, email);

    res.status(200).send('Заказ успешно оформлен! Детализация заказа отправлена на указанную почту.');
  } catch(err) {
      console.log('ОШИБКА: заказ не оформлен!');
      console.error(err);
      res.status(400).send(err);
  }

});

module.exports = router; 