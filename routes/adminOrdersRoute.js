const {Router} = require('express');
const router = Router();
const fs = require('fs')

const SefUrl = require('../models/sefurl');
const Category = require('../models/category');
const Product = require('../models/product');
const Order = require('../models/order');
const User = require('../models/user');
const Delivery = require('../models/delivery');

const listOfOrders = require('../lib/orders-query/orderList');
const details = require('../lib/orders-query/orderDetails')
const Filters = require('../lib/orderFilters');
const detailReport = require('../lib/orderDetailReport');
const ordersReport = require('../lib/ordersReport');

const menuLink = [
  {name: 'Категории', href: '/admin/categories',},
  {name: 'Товары', href: '/admin/products',},
  {name: 'Пользователи', href: '/admin/users',},
  {name: 'Заказы', href: '/admin/orders', active: true},
  {name: 'Характеристики', href: '/admin/properties',},
  {name: 'Доставка', href: '/admin/delivery',},
] 

const statusOpt = [
  {value: 'new', name: 'Новый'},
  {value: 'active', name: 'Открыт'},
  {value: 'agreed', name: 'Согласован'},
  {value: 'denied', name: 'Отменён'},
  {value: 'shipped', name: 'Отправлен'},
  {value: 'closed', name: 'Закрыт'},
];

const payStatus = [
  {name: 'Не оплачено', value: 'false'},
  {name: 'Оплачено', value: 'true'},
];

router.get('/orders', async (req, res) => {
  let processedOrders = [];
  let renderArr = [];
  try {
    const orders = await Order.find({},{},{sort:{date: -1}});
    processedOrders = await listOfOrders.getOrderList(orders);
  } catch (error) {
      console.error(error)
  }

  if(req.query.sort) {
    const qr = req.query.sort;
    const order = req.query.order;
    if (order === 'asc') {
      processedOrders.sort(byFieldASC(qr));
    } else {
      processedOrders.sort(byFieldDESC(qr));
    }
  }

  function filterItems(query, arr) {
    return arr.filter(function(el) {
      return el.toString().toLowerCase().indexOf(query.toLowerCase()) > -1;
    })
  }

  
  if (Object.keys(req.query).length > 0) {
    renderArr = Filters.orderFilter(processedOrders, req.query);
  } else {
    renderArr = processedOrders;
  }
  
  if (req.query.search) {
    const qr = req.query.search;
    let filteredOrders = [];
    for (order of processedOrders) {
      const valuesArray = Object.values(order);
      if (filterItems(qr, valuesArray).length > 0) {
        filteredOrders.push(order);
      }
    }
    renderArr = filteredOrders;
  }

  res.render('ap-orders', {
    title: 'Администратор: Заказы',
    orders: renderArr,
    menu: menuLink,
    link: req.originalUrl,
    statusOpt,
    payStatus,
    isAdmin: req.user.group === 'admin' ? true : false,
  });
});

router.get('/orders/record', async (req, res) => {
  const id = req.query.id
  try {
    let orderDetails = await details.getOrderDetail(id)
    if (req.query.report) {
      detailReport.createReport(orderDetails)
    }
    res.render('ap-order-record', {
      title: `Заказ № ${id.toUpperCase().substr(-8,8)}`,
      order: orderDetails,
      statusOpt, 
      payStatus,
    });
  } catch (error) {
    console.log(`ОШИБКА: загрузка детализации заказа ${id}`);
    console.error(error);
  }
});

// Запись деталей заказа
router.post('/order/record-edit', async (req,res) => {
  const id = req.query.id;
  const newStatus = req.body.status;
  const newPayment = req.body.payment;
  const newManagerComment = req.body.managerComment;

  try {
    await Order.findByIdAndUpdate(id, {status: newStatus, payment: newPayment, managerComment: newManagerComment});
  } catch (error) {
    console.error(error);
  }
  res.redirect(`/admin/orders/record?id=${id}`);
});

// Роут печати детализа
router.post('/order-details', async (req, res) => {
  const id = req.body.idToDetail
  try {
    let orderDetails = await details.getOrderDetail(id)
    detailReport.createReport(orderDetails, res);
  } catch (error) {
    console.log('ОШИБКА: формирование файла детализации');
    console.error(error);
  }
});

router.post('/orders/print-to-doc', async (req, res) => {
  const orders = await listOfOrders.getOrderList( await Order.find({},{},{sort:{date: 1}}) );
  const toPrint = Filters.getFilterToPrint(orders, req.body)
  ordersReport.createReport(toPrint, res, req.body.period);
});

// Поиск
router.post('/orders/search', async (req,res) => {
  const searchQuery = req.body.query;
  res.redirect(`/admin/orders?search=${searchQuery}`);
});

router.post('/orders/filter', async (req,res) => {
  const defLink = '/admin/orders';
  const dateStart = req.body['start-date'];
  const dateEnd = req.body['end-date'];
  const status = req.body.status;
  const payment = req.body.payment;

  let query = '?';

  if(dateStart !== '' && dateEnd !== '') {
    query += `start-date=${dateStart}&end-date=${dateEnd}&`
  }
  if (status !== 'none') {
    query += `status=${status}&`
  }
  if (payment !== 'none') {
    query += `payment=${payment}`
  }
  res.redirect(`${defLink}${query}`);
});

router.get('/orders/delete-record', async (req, res) => {
  const id = req.query.id;
  try {
    await Order.findByIdAndDelete(id);
    res.redirect('/admin/orders');
  } catch (error) {
    console.log('ОШИБКА: Не удалось удалить запись');
    console.log(error);
  }
});
function formatDate(date) {
  let dd = date.getDate();
  if (dd < 10) dd = '0' + dd;
  let mm = date.getMonth() + 1;
  if (mm < 10) mm = '0' + mm;
  let yy = date.getFullYear();

  let hh = date.getHours();
  if (hh < 10) hh = '0' + hh;
  let min = date.getMinutes();
  if (min < 10) min = '0' + min;
  
  return {
    date: `${dd}.${mm}.${yy}`,
    time: `${hh}:${min}`
  }
}

function byFieldDESC(field) {
  return (a, b) => a[field] > b[field] ? 1 : -1;
}
function byFieldASC(field) {
  return (a, b) => a[field] < b[field] ? 1 : -1;
}

module.exports = router;
