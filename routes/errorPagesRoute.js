const {Router} = require('express');
const router = Router();

router.get('/error/401', (req, res) => {
  res.render('401', {
    title: '401 Unauthorized'
  });
});

router.get('/error/403', (req, res) => {
  res.render('403', {
    title: '403 Forbidden'
  });
});

router.get('/error/404', (req, res) => {
  res.render('404', {
    title: '404 Not found'
  });
});

module.exports = router;