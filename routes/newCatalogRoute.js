const {Router} = require('express');
const router = Router(); 

const Product = require('../models/product');
const SefUrl = require('../models/sefurl');
const Property = require('../models/prodProperty');
const Delivery = require('../models/delivery');

router.get('/catalog', async (req,res) => {
    let productList = [];
    try {
        productList = await Product.find({hidden: false},{name: 1, cost: 1, img: 1});
    } catch (err) {
        throw err;
    }
    res.render('catalog', {
        title: 'Каталог',
        categories: global.categories,
        products: productList,
    });
});

router.get('/catalog/render', async (req, res) => {
    let element = req.query.elementId;
    let urlPart = [];
    try {
        urlPart = await SefUrl.findOne({elementId: element}, {_id: 0, link: 1});
        if(urlPart != null || undefined) {
            res.redirect(`/catalog${urlPart.link}`);
        } else {
            res.status(404).send('404: не найдено')
        }
        
    } catch(err) {
        console.error(err);
    } 
    
}); 

router.get('/catalog/:foo*', async (req, res) => {
    const filter = req.url.slice(8)
    let productList = [];
    let sefItem = {};
    let productId = '';
    try {
        sefItem = await SefUrl.findOne({link: filter});
        productId = sefItem.elementId;
        if (sefItem.type == 'ctg') {
            const linkFilter = await SefUrl.find({link: {$regex: filter}});
            if (Array.isArray(linkFilter)) {
                for (link of linkFilter) {
                  let prod = await Product.find({category: link.elementId, hidden: false});
                  if (Array.isArray(prod)) {
                      for (item of prod) {
                          productList.push(item); 
                      }
                  } else {
                      productList.push(prod);
                  }
                }
            } else {
                let prod = await Product.find({category: linkFilter.elementId, hidden: false})
                if (Array.isArray(prod)) {
                    for (item of prod) {
                        productList.push(item); 
                    }
                } else {
                    productList.push(prod);
                }
            }
            res.render('catalog', {
                title: 'Каталог',
                categories: global.categories,
                products: productList,
            });
        } else if(sefItem.type == 'prod') {
            const productItem = await Product.findOne({ _id: productId });
            const properties = await Property.find();
            const deliveryList = await Delivery.find();
            let params = []; 
            for (item of productItem.params) {
              for (prop of properties) {
                if (prop._id.toString() === item.name) {
                  params.push({
                    name: prop.name,
                    value: item.value.toString(),
                  })
                }
              }
            }
            res.render('product', {
                title: 'Страница Товара',
                product: productItem,
                params: params,
                delivery: deliveryList,
              
            });
        } else {
            res.send('Ошибка: не указан тип элемента в коллекции SefURL.');
            console.error('Ошибка: не указан тип элемента в коллекции SefURL.');
        }
    } catch(err) {
        console.error(err);
    }
});

module.exports = router