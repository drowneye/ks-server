const {Router} = require('express');
const router = Router(); 

const User = require('../models/user');
const Order = require('../models/order');
const filt = require('../lib/userFilter');
const genPassword = require('../lib/passwordUtils').genPassword;

const menuLink = [
  {name: 'Категории', href: '/admin/categories',},
  {name: 'Товары', href: '/admin/products',},
  {name: 'Пользователи', href: '/admin/users',   active: true},
  {name: 'Заказы', href: '/admin/orders',},
  {name: 'Характеристики', href: '/admin/properties',},
  {name: 'Доставка', href: '/admin/delivery',},
] 
const groupName = [
  {name: 'Все группы', value: ''},
  {name: 'Пользователь', value: 'user'},
  {name: 'Менеджер', value: 'manager'},
  {name: 'Администратор', value: 'admin'},
]

router.get('/users', async (req, res) => {    
  try { 
        let beArr = await User.find();
        const userReq = await filt.getFilteredUsers(req.query, groupName, beArr );
        
        res.render('ap-users', {
            title: 'Админ: Учетные записи',
            users: userReq,
            groupName, 
            menu: menuLink,
            isAdmin: req.user.group === 'admin' ? true : false,
        });
        
    } catch (error) {
        console.log(error);
    }
    
});

router.get('/users/user', async (req, res) => {
  
  try {
    const currentUser = await User.findById(req.query.id);
    res.render('ap-user', {
      title: 'Пользователь',
      user: currentUser,
      isAdmin: req.user.group === 'admin' ? true : false,
    });
  } catch (error) {
    console.log(error);
  }
});

router.post('/users/user-edit', async (req, res) => {
  console.log(req.body);
  const currentUser = req.body.current;
  const name = req.body.name;
  const email = req.body.email;
  const pass = req.body['new-password'];
  const pass2 = req.body['new-password-confirm'];
  const phone = req.body.tel;
  const address = req.body.address;
  const group = req.body.group;
  let saltHash;
  let salt = '';
  let hash = '';

  let editedUser = {
    name: name,
    email: email,
    phone: phone,
    address: address,
  }
  if (group) editedUser.group = group;
  if (pass && pass !== '') {
    saltHash = genPassword(pass);
    if(pass === pass2) {
      salt = saltHash.salt;
      hash = saltHash.hash;
    } else {
      res.status(403).send('Пароль и подтвердение пароля не совпадают');
    }
    editedUser.salt = salt
    editedUser.hash = hash
  }

  try {
    await User.findByIdAndUpdate(currentUser, editedUser);
    console.log(`Данные пользователя ${currentUser} обновлены`)
    res.redirect(`/admin/users/user?id=${currentUser}`);
  } catch (error) {
    console.log(error);
  }


});

router.get('/users/delete-user', async (req, res) => {
  const id = req.query.id;
  try {
    let orders = await Order.find({userId: id});
    if (orders.length > 0) {
      res.status(406).send('Данная учетная запись используется в заказах. Удаление невозможно!');
    } else {
      await User.findByIdAndDelete(id);
      console.log(`Учетная записть пользователя ${id} удалена!`);
      res.redirect('/admin/users');
    }
  } catch (error) {
    console.log('ОШИБКА: Удаление пользователя');
    console.error(error);
  }
});

module.exports = router