const {Router} = require('express');
const router = Router(); 

const Property = require('../models/prodProperty');

const menuLink = [
  {name: 'Категории', href: '/admin/categories',},
  {name: 'Товары', href: '/admin/products',},
  {name: 'Пользователи', href: '/admin/users',},
  {name: 'Заказы', href: '/admin/orders',},
  {name: 'Характеристики', href: '/admin/properties',  active: true},
  {name: 'Доставка', href: '/admin/delivery',},
] 

router.get('/properties', async (req,res) => {
  let proplist = [];
  try {
    proplist = await Property.find();
  } catch (error) {
    console.error(error);
  }
  res.render('ap-property', {
    title: 'Справочник: Параметры Товара',
    list: proplist,
    menu: menuLink,
  })
});

router.post('/properties/add', async (req,res) => {
  const name = req.body.new_name;
  try {
    const newProp = new Property({
      name: name,
    })
    await newProp.save();
    console.log(`Элемент ${newProp} добавлен.`);
  } catch (error) {
    console.error(error)
  }
  res.redirect('/admin/properties')
});

router.post('/properties/edit', async (req,res) => {
  const name = req.body.new_name;
  const current = req.body.edit_id;
  try {
    await Property.findByIdAndUpdate(current, {name: name});
    console.log(`Имя элемента ${current} изменено на ${name}.`);
  } catch (error) {
    console.error(error)
  }
  res.redirect('/admin/properties')
});

router.post('/properties/del', async (req,res) => {
  const current = req.body.delete;
  try {
    await Property.findByIdAndDelete(current);
    console.log(`Элемент ${current} удален.`);
  } catch (error) {
    console.error(error)
  }
  res.redirect('/admin/properties')
});

module.exports = router;