module.exports.isAuth =  (req, res, next) => {
    if (req.isAuthenticated()) {
        next()
    } else {
        res.status(401).redirect('/error/401');
    }
}

module.exports.isNotAuth =  (req, res, next) => {
  if (!req.isAuthenticated()) {
      next()
  } else {
      res.status(401).redirect('/users/profile');
  }
}

module.exports.isAdmin = (req, res, next) => {
    if (req.isAuthenticated() && req.user.group === 'admin') {
      next()
    } else {
        res.status(403).redirect('/error/403');
    }
}

module.exports.isManager = (req, res, next) => {
  if (req.isAuthenticated() && (req.user.group === 'admin' || req.user.group === 'manager')) {
    next()
  } else {
      res.status(403).redirect('/error/403');
  }
}
