const {Router} = require('express');
const passport = require('passport');
const genPassword = require('../lib/passwordUtils').genPassword;
const connection = require('../config/database');
const auth = require('../routes/authMw');
const router = Router();

// вызов моделей данных
const Category = require('../models/category');
const User = require('../models/user');

// Открытие страницы регистрации
router.get('/register', auth.isNotAuth, async (req,res) => {
    res.render('register', {
        title: "Регистрация",
        categories: global.categories
    });
    
});

// Обработка регистрации
router.post('/register', async (req, res) => {
  const saltHash = genPassword(req.body.pass);
  const name = req.body.name;
  const email = req.body.email;
  const pw = req.body.pass; // PassWord
  const pw2 = req.body.pass2;
  let salt;
  let hash;

  if (pw === pw2) {
    salt = saltHash.salt;
    hash = saltHash.hash;;
  } else {
    res.status(403).send('Пароль и подтвердение пароля не совпадают');
  }
  
  let newUser = new User({name, email, hash, salt});
  try {
    const isReg = await User.find({email: email});
    
    if (isReg.length > 0) {
      res.status(401).send('Указанный email уже зарегистрирован! <br> Введите другой, или свяжитесь с администрацией для восстановления доступа.');
    } else {
      newUser.save()
        .then((user) => {
          console.log(`Новый пользователь: ${user.name} -- ${user.email}`);
        })  
      res.redirect('/users/login')  
      }  
  } catch (error) {
    console.log('ОШИбКА при регисрации');
    console.error(error);
  }
});

// Страница логина
router.get('/login', async (req,res) => {
  res.render('login', {
      title: 'Логин',
  });
})

// Процесс логина 
router.post('/login', passport.authenticate('local', {
  failureRedirect: '/users/login',
  successRedirect: '/users/profile'
}), async (req, res, next) => {
    let userId;
    let currentUser;
    if (req.user) {
        userId = req.user._id;
        currentUser = await User.findById(userId);
        res.status(200).send({user: currentUser});
    } else {
        res.status(401).send('Указан неверный Логин / Пароль');
    } 
});

// Выход из профиля
router.get('/logout', (req, res, next) => {
  req.logout();
  res.redirect('/users/login')
});


module.exports = router;
