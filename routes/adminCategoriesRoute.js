const {Router} = require('express');
const router = Router(); 

const Category = require('../models/category');
const ctgModule = require('../lib/ctgModule');
const SefUrl = require('../models/sefurl');

const menuLink = [
  {name: 'Категории', href: '/admin/categories', active: true},
  {name: 'Товары', href: '/admin/products',},
  {name: 'Пользователи', href: '/admin/users'},
  {name: 'Заказы', href: '/admin/orders',},
  {name: 'Характеристики', href: '/admin/properties',},
  {name: 'Доставка', href: '/admin/delivery',},
] 

router.get('/categories', async (req, res) => {
  let forms = [];
    let topCtg = [];
    try {
        let topLevel = await Category.find({level: 1});
        let sortedCtg = await ctgModule.getSortedCategory(topLevel, []);

        for (ctg of sortedCtg) {
            topCtg.push({
                id: ctg._id.toString(),
                name: ctg.name,
                parent: ctg.parent 
            });
    
            forms.push({
                id: ctg._id.toString(),
                name: ctg.name,
                link: ctg.href,
                parentCat: ctg.parent
            });
        }
        res.render('ap-categories',{
            title: 'Адмминистрирование: Категории',
            topCategories: topCtg,
            forms: forms,
            menu: menuLink,
        });
    } catch(err) {
        console.error(err);
    }
}); 

router.post('/categories/add', async (req, res) => {
    const newName = req.body.new_name;
    const newLink = req.body.new_link;
    const newParent = req.body.new_parent; 
    let newLevel = {"_id": "", "level": 0};
    if (newParent) {
        try {
            newLevel = await Category.findById(newParent, {level: 1});
        } catch(err) {
            console.error(err);
        }
    }

    let newCtg = new Category({
        name: newName,
        href: newLink.toLowerCase(),
        parent: newParent,
        level: newLevel.level + 1,
    }); 

    let adress = '';
    try {
        if (newCtg.parent) {
            adress = await ctgModule.getFullUrl(newCtg.parent) + `/${newCtg.href}`;
        } else {
            adress = `/${newCtg.href}`;
        }
        let doublesCheck = await SefUrl.find({link: adress});
        let newSefurl = new SefUrl({
            elementId: newCtg._id,
            link: adress,
            type: 'ctg',
        });
        console.log(`Новый объект: ${newSefurl}`);
        if (doublesCheck == '') {
            newSefurl.save().then((url) => {console.log(url)});
            newCtg.save().then((category) => {console.log(category)});
            res.redirect('/admin/categories');
        } else {
            console.log('ВНИМАНИЕ: дублирование ссылок недопустимо!');
            res.status(423).send('ВНИМАНИЕ: дублирование ссылок недопустимо!');
        }
    } catch (error) {
        console.error(error);
    }
});

router.post('/categories/edit', async (req, res) => {
    const editName = req.body.edit_name;
    const editLink = req.body.edit_link;
    const editParent = req.body.edit_parent;
    const currentElement = req.body.current;

    let newLevel = {"_id": "", "level": 0};
    
    if (editParent) {
        try {
            console.log('Перезапись уровня вложенности');
            newLevel = await Category.findOne({_id: editParent}, {level: 1});
        } catch (error) {
            console.error(error);
        }
    };

    let editCtg = {
        name: editName,
        href: editLink.toLowerCase(),
        parent: editParent,
        level: newLevel.level + 1,
    };
    console.log(`Объект редактирования: ${editCtg}`);
    let adress = '';
    try {
        if (editCtg.parent) {
            adress = await ctgModule.getFullUrl(editCtg.parent) + `/${editCtg.href}`;
        } else {
            adress = `/${editCtg.href}`;
        }
        if (ctgModule.checkForDoubles(currentElement, adress)) {
            await SefUrl.findOneAndUpdate({elementId: currentElement}, {link: adress});
            await Category.findOneAndUpdate({_id: currentElement}, editCtg);
            
            await ctgModule.updateLevel(currentElement);
            await ctgModule.updateChildURL(currentElement);
            console.log('Записи обновлены.');
            await ctgModule.updateProductsURL(currentElement);
        } else {
            console.log('ОШИБКА: Дублирование ссылок ЧПУ недопустимо!');
            res.status(409).send('ОШИБКА: Дублирование ссылок ЧПУ недопустимо!');
        }
    } catch (error) {
        console.log('---------- Error on edit -------------');
        console.error(error);
    }

    res.redirect('/admin/categories');
});

router.post('/categories/del', async (req, res) => {
    const currentElement = req.body.delete;
    try {
      await ctgModule.updateProdOnDelete(currentElement);
      await ctgModule.updateOnDelete(currentElement);
      await Category.findOneAndDelete({_id: currentElement});
      await SefUrl.findOneAndDelete({elementId: currentElement});    
      console.log(`Элемент: ${currentElement} - успешно удален.`);
    } catch (error) {
        console.error(error)
    }
    res.redirect('/admin/categories');
});

module.exports = router