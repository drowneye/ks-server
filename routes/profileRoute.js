const {Router} = require('express');

const User = require('../models/user');
const Order = require('../models/order');
const Product = require('../models/product');
const router = Router();

router.get('/profile', async (req, res, next) => {
  if(!global.user) {
    res.redirect('/users/login');
  } else {
    const userId = global.user._id;
    let currentUser = await User.findById(userId);

    let userOrders = []
        , massOrder = []
        , orders = [];

    userOrders = await Order.find({userId: userId}); // отбор заказов текущего пользователя
    userOrders.forEach((item) => { 
      massOrder.push({
        date: formatDate(item.date),
        orderlist: item.orderlist
      });
    });
      
    for (item of massOrder)  {
      let prod, arr = [];

      for (tov of item.orderlist) {
        prod =  await Product.findById(tov.id,).catch((err) => {throw err});
        arr.push({
          id: tov.id,
          name: prod.name,
          count: tov.count,
          cost: prod.cost
        });
      }
      orders.push({
        date: item.date,
        products: arr
      });
    }
      //console.log(orders);
      res.render('profile',{
          title: 'Личный кабинет',
          crUser: currentUser,
          orders: orders,
          isAdmin: (req.user.group === 'admin' || req.user.group === 'manager') ? true : false,
      });
  }
});


router.post('/profile', async (req, res, next) => {
  const name = req.body.name;
  const phone = req.body.phone;
  const email = req.body.email;
  const address = req.body.address;
  const userId = global.user._id;

  await User.findByIdAndUpdate(userId, {$set: {
    name: name,
    phone: phone,
    email: email,
    address: address
  }}, {useFindAndModify: false});
  
  res.redirect('/users/profile');
});


function formatDate(date) {

  let dd = date.getDate();
  if (dd < 10) dd = '0' + dd;

  let mm = date.getMonth() + 1;
  if (mm < 10) mm = '0' + mm;

  let yy = date.getFullYear(); 

  return dd + '.' + mm + '.' + yy;
  }

module.exports = router;