const {Router} = require('express');
// Модели данных
const router = Router();
router.get('/', async (req,res, next) => {
    res.render('index', {
        title: 'Ножики | Режики',
        categories : global.categories,
      }); 
});

module.exports = router;