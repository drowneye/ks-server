const {Router} = require('express');
const sizeOf = require('image-size');
const router = Router(); 

const Category = require('../models/category');
const Order = require('../models/order');
const upload = require('../lib/upload');
const SefUrl = require('../models/sefurl');
const Product = require('../models/product');
const imgProc = require('../lib/imgProcessing');
const Property = require('../models/prodProperty');

const stat = require('../lib/statistic');
const statReport = require('../lib/statisticsReport');

const menuLink = [
  {name: 'Категории', href: '/admin/categories',},
  {name: 'Товары', href: '/admin/products', active: true},
  {name: 'Пользователи', href: '/admin/users',},
  {name: 'Заказы', href: '/admin/orders',},
  {name: 'Характеристики', href: '/admin/properties',},
  {name: 'Доставка', href: '/admin/delivery',},
] 

router.get('/products', async (req,res) => {
  let productList = [];
  let renderArr = [];
  try {
    
    if (req.query.search) {
      const qr = req.query.search;
      productList = await Product.find({$text: {$search: qr}}).sort({ts: -1});
    } else {
      productList = await Product.find().sort({ts: -1});
    }

  } catch (error) {
    console.error(error);
  }
  res.render('ap-product', {
    title: 'Администрирование: Товары',
    products: productList,
    menu: menuLink,
    isAdmin: req.user.group === 'admin' ? true : false,
  });
});

// Страница добавления нового товара с пустыми полями
router.get('/products/new', async (req, res) => {
  const emptyProd = {
    name: '',
    translit: '',
    desc: '',
    category: '',
    img: {
      name: 'plug',
      ext: ['.jpg'],
    },
    cost: '',
    params: [],
  }
  let propList = [];
  let ctgList = [];

  try {
    propList = await Property.find();
    ctgList = await Category.find({},{_id: 1, name: 1});

    
  } catch (error) {
    console.error(error);
  }
  res.render('ap-product-item', {
    title: `Товар №`,
    prod: emptyProd,
    category: ctgList,
    properties: propList,
    form: {
      act: '/admin/products/item-add',
      name: 'product-edit'
    },
  })
});
// Запрос на добавление нового товара
router.post('/products/item-add', upload.single('new-image'),async (req, res) => {
  const name = req.body.name;
  const translit = req.body.code;
  const category = req.body.category;
  const cost = req.body.price;
  const desc = req.body.desc;
  let params = [];
  let img = {
    name: 'plug',
    ext: ['.jpg'],
  };
  try {
    const propList = await Property.find();
     for (prop of propList) {
       if (req.body[prop._id]){
         params.push({
           name: prop._id.toString(),
           value: req.body[prop._id],
         })
       }
     }
    if (req.file) {
      console.log(`uploaded file: ${req.file.filename}`);
      const dms = sizeOf(req.file.path);
  
      if (dms.width >= 552*3) {
        img = imgProc.resize3x(req.file);
      } else if(dms.width >= 552*2 && dms.width < 552*3) {
        img = imgProc.resize2x(req.file);
      } else if (dms.width >= 552 && dms.width < 552*2) {
        img = imgProc.resize1x(req.file);
      } else if (dms.width < 552) {
        img = imgProc.resizeNone(req.file);
      }
    } else {
      console.log('File did not found');
    }
    
    const prod = new Product({
      name, translit, category, cost, desc, params, img,
    });
    const linkPart = await SefUrl.findOne({elementId: category});
    const SefUrlLink = new SefUrl({
      elementId: prod._id,
      link: `${linkPart.link}/${translit}`,
      type: 'prod',
    })

    await prod.save();
    await SefUrlLink.save()
    console.log(prod)
   
    res.redirect(`/admin/products/item?id=${prod._id}`);
  } catch (error) {
    console.log('ОШИБКА: запрос на изменение товара');
    console.error(error);
  }
});
// Страница редактирования товара из списка
router.get('/products/item', async (req, res) => {
  const id = req.query.id;
  let propList = [];
  let productItem = {};
  

  try {
    propList = await Property.find();
    productItem = await Product.findById(id);
    ctgList = await Category.find({},{_id: 1, name: 1});

  } catch (error) {
    console.error(error);
  }
  res.render('ap-product-item', {
    title: `Товар №`,
    prod: productItem,
    category: ctgList,
    properties: propList,
    form: {
      act: '/admin/products/item-edit',
      name: 'product-edit'
    },
  })
});

// Запрос на изменение товара
router.post('/products/item-edit', upload.single('new-image'),async (req, res) => {
  const name = req.body.name;
  const translit = req.body.code;
  const category = req.body.category;
  const cost = req.body.price;
  const desc = req.body.desc;
  let params = [];
  let img = {
    name: 'plug',
    ext: ['.jpg'],
  };
  try {
    const propList = await Property.find();
     for (prop of propList) {
       if (req.body[prop._id]){
         params.push({
           name: prop._id.toString(),
           value: req.body[prop._id],
         })
       }
     }
    if (req.file) {
      console.log(`uploaded file: ${req.file.filename}`);
      const dms = sizeOf(req.file.path);
  
      if (dms.width >= 552*3) {
        img = imgProc.resize3x(req.file);
      } else if(dms.width >= 552*2 && dms.width < 552*3) {
        img = imgProc.resize2x(req.file);
      } else if (dms.width >= 552 && dms.width < 552*2) {
        img = imgProc.resize1x(req.file);
      } else if (dms.width < 552) {
        img = imgProc.resizeNone(req.file);
      }
    } else {
      console.log('File did not found');
    }
    
    const prod = {
      name, translit, category, cost, desc, params, img,
    } 
    const linkPart = await SefUrl.findOne({elementId: category});
    console.log(prod);
    await Product.findByIdAndUpdate(req.body.id, prod);
    await SefUrl.findOneAndUpdate({elementId: req.body.id }, {link: `${linkPart.link}/${translit}`});
    res.redirect(`/admin/products/item?id=${req.body.id}`);
  } catch (error) {
    console.log('ОШИБКА: запрос на изменение товара');
    console.error(error);
  }
});

router.get('/products/item-hide', async (req,res) => {
  const id = req.query.id;
  try {
    await Product.findByIdAndUpdate(id, {hidden: true});
    console.log(`Товар ${id} скрыт`);
    res.redirect('/admin/products');
  } catch (error) {
    console.log('ОШИБКА: запрос на скрытие товара');
    console.error(error);
  }
});

router.get('/products/item-show', async (req,res) => {
  const id = req.query.id;
  try {
    await Product.findByIdAndUpdate(id, {hidden: false});
    console.log(`Товар ${id} открыт`);
    res.redirect('/admin/products');
  } catch (error) {
    console.log('ОШИБКА: запрос на скрытие товара');
    console.error(error);
  }
});

router.get('/products/item-del', async (req,res) => {
  const id = req.query.id;
  try {
    let orders = await Order.find();
    for (item of orders){
      for(el of item.orderlist) {
        if (el.id === id) {
          res.status(406).send('Запись используется в заказах. Удаление невозможно!')
        }
      }
    }
    await Product.findByIdAndDelete(id);
    await SefUrl.findOneAndDelete({elementId: id});
    console.log(`Товар ${id} удален`);
    res.redirect('/admin/products');
  } catch (error) {
    console.log('ОШИБКА: запрос на удаление товара');
    console.error(error);
  }
});

router.post('/products/statistics', async (req,res) => {
  try {
    const period = req.body.period;
    const counted = await stat.getStatistics(period);
    statReport.createReport(counted, period,res);
  
  } catch (error) {
    console.log('ОШИБКА: Запрос документя статистики');
    console.error(error);
  }
  
});

function formatDate(date) {
  let dd = date.getDate();
  if (dd < 10) dd = '0' + dd;
  let mm = date.getMonth() + 1;
  if (mm < 10) mm = '0' + mm;
  let yy = date.getFullYear();

  let hh = date.getHours();
  if (hh < 10) hh = '0' + hh;
  let min = date.getMinutes();
  if (min < 10) min = '0' + min;

  return `${dd}.${mm}.${yy} ${hh}:${min}`;
}


module.exports = router;