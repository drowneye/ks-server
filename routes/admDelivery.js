const {Router} = require('express');
const router = Router(); 

const Delivery = require('../models/delivery');

const menuLink = [
  {name: 'Категории', href: '/admin/categories',},
  {name: 'Товары', href: '/admin/products',},
  {name: 'Пользователи', href: '/admin/users',},
  {name: 'Заказы', href: '/admin/orders',},
  {name: 'Характеристики', href: '/admin/properties',},
  {name: 'Доставка', href: '/admin/delivery', active: true},
] 

router.get('/delivery', async (req, res) => {
  let deliveryList = [];
  try {
    deliveryList = await Delivery.find();
  } catch (error) {
    console.error(error);
  }
  
  res.render('ap-delivery', {
    title: 'Доставка',
    list: deliveryList,
    menu: menuLink,
  })
});

router.post('/delivery/add', async (req, res) => {
  const name = req.body.new_name;
  const cost = req.body.new_cost;

  const deliveryItem = new Delivery({
    name: name,
    cost: cost,
  });

  await deliveryItem.save();

  res.redirect('/admin/delivery');
});

router.post('/delivery/edit', async (req, res) => {
  const name = req.body.new_name;
  const cost = req.body.new_cost;
  const currentItem = req.body.edit_id;
  const deliveryItem = {
    name: name,
    cost: cost,
  };

  await Delivery.findByIdAndUpdate(currentItem, deliveryItem);

  res.redirect('/admin/delivery');
});

router.post('/delivery/del', async (req, res) => {
  const currentItem = req.body.delete;
  try {
    await Delivery.findByIdAndDelete(currentItem);
    console.log(`Элемент ${currentItem} удален.`);  
  } catch (error) {
    console.error(error);
  }
  res.redirect('/admin/delivery');
});
module.exports = router;